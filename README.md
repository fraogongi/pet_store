# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

#### Dependencies ####

* Python 3.4
* Django 1.7.1

#### Local Installation ####
Using `pip` and `virtualenv` is highly recommended so that you do not run into any conflicts with other python applications you may be running now or in the future.

Assuming that you already have `git` installed, next you need to have `pip`, and `virtualenv` installed. To do that, simply run:

```
$ easy_install pip
$ pip install virtualenv
$ pip install virtualenvwrapper
```

Next, you need to create a `virtualenvironment`, and install this application along with its dependencies. For that, you could do something like:

```
$ mkvirtualenv petstore
$ workon petstore
```

Now you're ready to clone this repo and get working:

```
$ git clone https://fraogongi@bitbucket.org/fraogongi/pet_store.git
$ cd pet_store
```

Next, depending on the environment you are currently working on, you need to install all of the dependencies:

```
$ pip install -r requirements/development.txt
```

Next, configure your local settings by copying local_settings.txt to local_settings.py and filling in all of the necessary fields:

```
$ cd ..
$ cp settings/local_settings.txt settings/local_settings.py
$ nano settings/local_settings.py
```

Next, you'll need to create your database:

```
$ createdb petstore_dev
$ python manage.py syncdb
$ python manage.py migrate
```

And then load the initial data, by running the following command:

```
$ python manage.py loaddata initial_data.json
```

Once you've done that you can run your server:

```
$ python manage.py runserver
```

If you navigate to `http://127.0.0.1:8000/` you should see the application, and if you navigate to `http://127.0.0.1:8000/admin` you should be able to add some entries  to the application.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
