from random import randint

from django.core.urlresolvers import resolve, reverse
from django.test import TestCase
from django.test.client import Client
from apps.adverts.views import home_page

from accountusers.models import AuthUser
from .models import Advert, AdCategory

def category_factory():
    return AdCategory.objects.create(title='Category-' + str(randint(2,100)))

class AdvertsTest(TestCase):

    def setUp(self):
        self.c = Client()
        self.user = AuthUser.objects.create_user(username="test", email="test@test.com", password="test")
        self.user.is_active = True
        self.user.save()
        
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)
        
    def test_adverts_access(self):
        response = self.c.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        
    def test_adverts_context_is_populated_in_template(self):
        #create a few adverts
        Advert.objects.create(title='Advert 1', owner=self.user, category=category_factory())
        Advert.objects.create(title='Advert 2', owner=self.user, category=category_factory())

        response = self.c.get(reverse('home'))
        #assert that context contains as many adverts as we expect
        self.assertEqual(len(response.context['adverts']), 2)
        
    def test_entry_create(self):
        response = self.c.get(reverse('new_advert'))
        self.assertEqual(response.status_code, 302)

        self.c.login(username='test', password='test')
        response = self.c.get(reverse('new_advert'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver/login/?next=/ads/new')
