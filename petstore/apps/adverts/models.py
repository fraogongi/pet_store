import os
import pytz
import uuid
from datetime import datetime, timedelta

from django.db import models
from django.db.models import Count
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from .fixtures import COUNTY_LOCATIONS

utc = pytz.utc


class AdCategoryManager(models.Manager):

    def get_all_children(self, parentCat, include_self=True):
        subcat_list = []
        if include_self:
            subcat_list.append(parentCat)
        for category in self.filter(parent=parentCat):
            # using extend instead of append to create only one list, 
            # and not a list of lists
            subcat_list.extend(self.get_all_children(category, include_self=True))
        return subcat_list


class AdCategory(models.Model):
    title = models.CharField(_("Title"), max_length=100)
    slug = models.SlugField(max_length=100, db_index=True)
    parent = models.ForeignKey('self', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    
    objects = AdCategoryManager()
    
    class Meta:
        unique_together = ('slug', 'parent',)
        ordering = ['-title']

    def __str__(self):
        return self.title
        
    def get_absolute_url(self):
        return reverse('view_category', args=[self.id])
        
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(self.title)

        super(AdCategory, self).save(*args, **kwargs)


class AdvertManager(models.Manager):

    def all_approved_ads(self, **kwargs):
        return self.filter(status=1, **kwargs)
        
    def all_expired_ads(self, **kwargs):
        """
        Returns expired adverts
        """
        qs = super(AdvertManager, self).get_queryset()
        for advert in qs:
            if not advert.isexpired():
                # TODO: This is not very efficient. Results in a new queryset each time.
                qs = qs.exclude(pk=advert.pk)
        return qs.filter(**kwargs)
        
    def all_active_and_approved_ads(self):
        """
        Returns adverts that are NOT expired, and are approved
        """
        qs = self.all_approved_ads()
        for row in qs:
            if row.isexpired():
                # TODO: This is not very efficient. Results in a new queryset each time.
                qs = qs.exclude(pk=row.pk)
        return qs
        
    def get_user_ads(self, owner):
        return self.all_active_and_approved_ads().filter(owner=owner)
        
    def get_popular_ads(self, category):
        """
        Fetches popular ads for the given category (and all its subdirectories) 
        by counting the number of hitcounts on the advert.
        """
        sub_categories = AdCategory.objects.get_all_children(category)
        qs = self.all_active_and_approved_ads().filter(category__in=sub_categories)
        qs_popular = qs.annotate(num_view_count=Count('adverthitcounts')).order_by('-num_view_count')[:15]
        return qs_popular
        
    def get_premium_ads(self, category=None):
        """
        Fetches premium ads for the given category (and all its subdirectories) 
        if given.
        """
        if category:
            sub_categories = AdCategory.objects.get_all_children(category)
            qs = self.all_active_and_approved_ads().filter(is_featured=True, category__in=sub_categories)
        else:
            qs = self.all_active_and_approved_ads().filter(is_featured=True)
        return qs
        


class Advert(models.Model):

    # Ad Types
    PRICE = 1
    WANTED = 2
    TRADE = 3
    FREE = 4
    CONTACT = 5
    
    ADTYPES = (
        (PRICE, 'Priced Ad'),
        (WANTED, 'Item Wanted'),
        (TRADE, 'Will Barter/Trade'),
        (FREE, 'Free'),
        (CONTACT, 'Please Contact')
    )
    
    # Advert states
    PENDING = 0
    APPROVED = 1
    CANCELLED = 2

    ADVERT_STATES = (
        (PENDING, 'Pending approval'),
        (APPROVED, 'Approved'),
        (CANCELLED, 'Cancelled'),
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True, blank=False, null=False)
    title = models.CharField(_("Title"), max_length=120)
    slug = models.SlugField(max_length=100, db_index=True)
    description = models.TextField(_("Description"), default='')
    category = models.ForeignKey(AdCategory, db_index=True, related_name='ad_category', verbose_name=_('Category'))
    date_posted = models.DateTimeField(_('Date Posted'), db_index=True, default=datetime.now, auto_now_add=True)
    date_last_modified = models.DateTimeField(_('Date Last Modified'), 
                            db_index=True, null=True, blank=True, 
                            default=datetime.now, auto_now=True)
    status = models.IntegerField(choices=ADVERT_STATES, default=0)
    adtype = models.IntegerField(choices=ADTYPES, default=1)
    price = models.DecimalField(_('Price'), max_digits=12, decimal_places=2, blank=True, default=0,
                            help_text=_('Price'), validators=[MinValueValidator(0)])
    location = models.IntegerField(choices=COUNTY_LOCATIONS, default=30) # Defaults to Nairobi County
    expire_date = models.DateTimeField(_('Expiry Date'), null=True, blank=True)
    is_featured = models.BooleanField(default=False) # Same as premium i.e. is_premium
    
    objects = AdvertManager()
    
    class Meta:
        ordering = ('-date_posted',)
        verbose_name = _("Advert")
        verbose_name_plural = _("Adverts")
        
    def __str__(self):
        return self.title
        
    def get_absolute_url(self):
        return reverse('view_advert', args=[self.slug, self.id])
        
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(self.title)
            
            # Set the initial advert status to pending approval
            self.status = 0
        
            # Set the expiration date 
            advert_activation_days = getattr(settings, 'ADVERT_VALID_DAYS', 30)

            if advert_activation_days:
                self.expire_date = datetime.now() + timedelta(days=advert_activation_days)

        super(Advert, self).save(*args, **kwargs)
        
    def isexpired(self):
        is_expired = False
        if utc.localize(datetime.now()) > self.expire_date:
            is_expired = True 
        return is_expired
        
        
def get_extension(filename):
    return os.path.splitext(filename)[1]
    

class AdvertImage(models.Model):

    def generate_new_filename(instance, filename):
        """
        Generate a new name for the given file
        """
        IMAGE_UPLOAD_DIR = "advert_images"
        old_fname, extension = os.path.splitext(filename)
        return '%s/%s%s' % (IMAGE_UPLOAD_DIR, uuid.uuid4().hex, extension)
    
    
    advert = models.ForeignKey(Advert, related_name='images')
    title = models.CharField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to=generate_new_filename, null=True, blank=True)
    
    def __str__(self):
        return self.image.name or 'noname'
        
    def delete(self, *args, **kwargs):
        self.image.delete(False)
        super(AdvertImage, self).delete(*args, **kwargs)
        
        
class AdvertHitCountManager(models.Manager):

    def hits_in_last(self, **kwargs):
        """
        Returns hit count for an Advert during a given time period.

        This will only work for as long as hits are saved in the database.
        If you are purging your database after 45 days, for example, that means
        that asking for hits in the last 60 days will return an incorrect
        number, since the longest period it can search will be 45 days.
        
        For example: hits_in_last(days=7).

        Accepts days, seconds, microseconds, milliseconds, minutes, 
        hours, and weeks.  It's creating a datetime.timedelta object.
        """
        period = datetime.utcnow() - datetime.timedelta(**kwargs)
        return self.filter(created__gte=period).count()


class AdvertHitCount(models.Model):
    """
    Model captures a single Hit by a visitor.
    
    Depending on how long you want to be able to use 'Hit.hits_in_last(days=30)' 
    you should probably also occasionally clean out this database using a cron job.

    It could get rather large.
    """
    advert = models.ForeignKey(Advert, related_name='adverthitcounts')
    ip = models.CharField(max_length=40)
    session = models.CharField(max_length=50)
    created = models.DateTimeField(default=datetime.now, auto_now_add=True)
    
    objects = AdvertHitCountManager()
    
    class Meta:
        ordering = ( '-created', )    
        get_latest_by = 'created'
        
    def __str__(self):
        return u'Hit: %s' % self.pk 
    
