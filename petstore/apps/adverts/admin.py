from django.contrib import admin
from .models import AdCategory, Advert, AdvertImage, AdvertHitCount


class AdCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'parent')
    list_filter = ('parent',)
    search_fields = ('title',)
    
    
class AdvertAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'owner', 'category', 'date_posted', 'status', 'adtype')
    list_filter = ('status', 'owner')
    search_fields = ('title',)
    
    
class AdvertImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'advert', 'image')


class AdvertHitCountAdmin(admin.ModelAdmin):
    list_display = ('id', 'advert', 'ip', 'created')
    list_filter = ('advert',)

admin.site.register(AdCategory, AdCategoryAdmin)
admin.site.register(Advert, AdvertAdmin)
admin.site.register(AdvertImage, AdvertImageAdmin)
admin.site.register(AdvertHitCount, AdvertHitCountAdmin)
