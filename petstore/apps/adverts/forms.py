from django.forms.models import inlineformset_factory
from django import forms
from django.forms import ModelForm, RadioSelect, TextInput

from .models import Advert, AdvertImage

class AdvertForm(ModelForm):

    class Meta:
        model = Advert
        fields = ('title', 'description', 'category', 'price', 'adtype', 'location')
        widgets = {
            'adtype': RadioSelect(),
            'price': TextInput(),
        }


class AdvertCategoryForm(ModelForm):
    """
    Form used in FormWizard for selecting categories
    """

    class Meta:
        model = Advert
        fields = ('category',)


class AdvertDetailsForm(ModelForm):
    """
    Form used in FormWizard for adding advert details.
    """

    class Meta:
        model = Advert
        fields = ('title', 'description', 'location', 'adtype', 'price')
        widgets = {
            'adtype': RadioSelect(),
            'price': TextInput(),
        }


class AdvertImageForm(ModelForm):

    class Meta:
        model = AdvertImage
        fields = ('image',)

AdvertImageFormset = inlineformset_factory(Advert, AdvertImage, fields=('image',), can_delete=False, extra=3, max_num=3)


class ContactForm(forms.Form):
    name = forms.CharField(required=True, widget=forms.TextInput(attrs={'placeholder':'Name'}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'placeholder':'Email'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder':'Message'}))

# NOTE: If you change the name of the form, be sure to also change the JS variable in the template.
FORMS = [("ad_category", AdvertCategoryForm),
         ("ad_details", AdvertDetailsForm),
         ("ad_images", AdvertImageFormset)]

TEMPLATES = {"ad_category": "adverts/advert_category_step.html",
             "ad_details": "adverts/advert_details_step.html",
             "ad_images": "adverts/advert_images_step.html"}