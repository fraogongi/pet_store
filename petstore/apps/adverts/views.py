import datetime
import json
import os

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail, mail_admins
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, redirect, render, render_to_response

from apps.adverts.forms import AdvertForm, AdvertImageFormset, ContactForm, FORMS, TEMPLATES
from apps.adverts.models import Advert, AdvertImage, AdCategory, AdvertHitCount
from .lib import LoginRequiredMixin


def get_root_parent_categories():
    """
    Get the main categories, the ones that have no parent.
    """
    return AdCategory.objects.filter(parent=None)


def get_ads_by_category(category):
    """
    Fetch all adverts that are 'ACTIVE' and are 'APPROVED' and falls within
    the given category or any of its sub-directories.
    """
    category = get_object_or_404(AdCategory, title=category.title)
    sub_categories = AdCategory.objects.get_all_children(category)
    adverts = Advert.objects.all_active_and_approved_ads().filter(category__in=sub_categories)
    return adverts


def services(request):
    category = get_object_or_404(AdCategory, title='Pet Care and Services')
    adverts = get_ads_by_category(category)
    return render(request, 'services.html', {
                'adverts': adverts,
                'query': category,
                'category': category})


def products(request):
    category = get_object_or_404(AdCategory, title='Pet Products')
    adverts = get_ads_by_category(category)
    return render(request, 'products.html', {
                'adverts': adverts,
                'query': category,
                'category': category})


def home_page(request):
    category = get_object_or_404(AdCategory, title='Pets For Sale')
    adverts_list = get_ads_by_category(category)
    popular_adverts = Advert.objects.get_popular_ads(category=category)
    premium_adverts = Advert.objects.get_premium_ads(category=category)

    adverts_paginator = Paginator(adverts_list, settings.PAGINATOR_SIZE)

    page = request.GET.get('page')
    try:
        adverts = adverts_paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        adverts = adverts_paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        adverts = adverts_paginator.page(adverts_paginator.num_pages)

    return render(request, 'home.html', {
        'adverts': adverts,
        'popular_adverts': popular_adverts,
        'premium_adverts': premium_adverts,
        'parent_categories': get_root_parent_categories(),
        'query': category,
        'category': category
    })


@login_required
def add_new_advert(request):
    """
    REMOVE: This function has been replaced with the AdvertWizard CBV.
    Will remove after verification that the CBV version works without hitches.
    """
    form = AdvertForm()
    img_formset = AdvertImageFormset(instance=Advert())

    if request.method == 'POST':
        form = AdvertForm(data=request.POST)
        if form.is_valid():
            advert = form.save(commit=False)
            advert.owner = request.user
            img_formset = AdvertImageFormset(request.POST, request.FILES, instance=advert)
            if img_formset.is_valid():
                advert.save()
                img_formset.save()
                return redirect(advert)

            # TODO: Notify user that his/her advert is pending approval
            user.email_user("Your advert is pending approval", "You recently posted an advert on PetsFirst Kenya. A moderator will soon review your advert.", settings.DEFAULT_FROM_EMAIL)

            # TODO: Notify admins to approve the advert
            mail_admins("New Advert pending approval", "A new advert is pending your approval.")

    # For the sidebar category menu list
    category = get_object_or_404(AdCategory, title='Pets For Sale')
    return render(request, "adverts/new_advert.html", {
        'form': form,
        'parent_categories': get_root_parent_categories(),
        'img_formset': img_formset,
        'category': category,
        'action': "Create"
    })


class AdvertWizard(LoginRequiredMixin, SessionWizardView):

    form_list = FORMS
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT))

    def get_template_names(self):
        """
        Returns a list of template names to be used for the request.
        """
        return [TEMPLATES[self.steps.current]]

    def get_context_data(self, form, **kwargs):
        context = super(AdvertWizard, self).get_context_data(form=form, **kwargs)
        if self.steps.current == 'ad_category':
            context.update({'parent_categories': get_root_parent_categories()})
        return context

    def done(self, form_list, **kwargs):
        advert = Advert()
        form_count = 0
        for form in form_list:
            if form_count == 2: # This is the ad_images form
                # Since this is the third form, all the data relating to the
                # Advert has been captured and we can proceed to save the Advert.
                # This is so that we can use the saved Advert in the AdvertImage objects.
                advert.owner = self.request.user
                advert.save()

                for img_dict in form.cleaned_data: # Apparently this is a list instead of a dict
                    advert_image = AdvertImage()
                    for field, value in iter(img_dict.items()):
                        setattr(advert_image, field, value)
                    advert_image.advert = advert
                    advert_image.save()
            else:
                # Capture the Advert details
                for field, value in iter(form.cleaned_data.items()):
                    setattr(advert, field, value)
            form_count += 1

            # TODO: Notify user that his/her advert is pending approval
            self.request.user.email_user("Your advert is pending approval",
                                         "You recently posted an advert on PetsFirst Kenya. A moderator will soon review your advert.",
                                         settings.DEFAULT_FROM_EMAIL)

            # TODO: Notify admins to approve the advert
            mail_admins("New Advert pending approval",
                        "A new advert is pending your approval.")

        return redirect(advert)


@login_required
def update_advert(request, advert_id):
    advert = get_object_or_404(Advert, pk=advert_id)

    form = AdvertForm(instance=advert)
    img_formset = AdvertImageFormset(instance=advert)

    if request.method == 'POST':
        form = AdvertForm(request.POST, instance=advert)
        if form.is_valid():
            advert = form.save(commit=False)
            img_formset = AdvertImageFormset(request.POST, request.FILES, instance=advert)
            if img_formset.is_valid():
                advert.save()
                img_formset.save()
                return redirect(advert)

    return render(request, "adverts/new_advert.html", {
        'form': form,
        'parent_categories': get_root_parent_categories(),
        'img_formset': img_formset,
        'action': "Update"
    })


def view_advert(request, advert_slug, advert_id):
    advert = get_object_or_404(Advert, pk=advert_id)
    seller_contact_form = ContactForm()

    js = "jQuery.post( '" + reverse('hitcount_update_ajax') + "',"   + \
            "\n\t{ advert_pk : '" + str(advert.pk) + "', csrfmiddlewaretoken: csrf_token },\n"         + \
            "\tfunction(data, status) {\n"                         + \
            "\t\tif (data.status == 'error') {\n"                  + \
            "\t\t\tconsole.log(data.status);\n"                   + \
            "\t\t}\n\t},\n\t'json');"

    return render(request, 'adverts/view_advert.html', {
        'advert': advert,
        'seller_contact_form': seller_contact_form,
        'hitcount_js': js
    })


def _update_hit_count(request, advert_id):
    """
    This is NOT a view!  But should be used within a view.

    Returns True if the request was considered a Hit; returns False if not.
    """
    advert = get_object_or_404(Advert, pk=advert_id)
    advert_hit_count = AdvertHitCount.objects.filter(
                                        advert=advert,
                                        session=request.session.session_key)
    if not advert_hit_count:
        hitcount_obj = AdvertHitCount(advert=advert,
                                  ip=request.META['REMOTE_ADDR'],
                                  session=request.session.session_key)
        hitcount_obj.save()
        return True
    return False


def json_error_response(error_message):
    return HttpResponse(json.dumps(dict(success=False,
                        error_message=error_message)))


def update_hit_count_ajax(request):
    """
    Ajax call that can be used to update a hit count.
    """
    # make sure this is an ajax request
    if not request.is_ajax():
        raise Http404()

    if request.method == "GET":
        return json_error_response("Hits counted via POST only.")

    advert_pk = request.POST.get('advert_pk')
    result = _update_hit_count(request, advert_pk)
    if result:
        status = "success"
    else:
        status = "error"

    return HttpResponse(json.dumps({'status': status}),
                        content_type='application/json'
    )


def contact_seller(request, advert_id):
    if request.method == 'POST':
        form = ContactForm(data=request.POST)
        advert = get_object_or_404(Advert, pk=advert_id)

        if form.is_valid():
            name = form.cleaned_data['name']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['email']
            subject = "[Classified Site] Buyer Enquiry"

            advert.owner.email_user(subject, message, sender)
            #send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/')


def contact_us(request):
    contact_form = ContactForm()
    if request.method == 'POST':
        contact_form = ContactForm(data=request.POST)

        if contact_form.is_valid():
            name = contact_form.cleaned_data['name']
            message = contact_form.cleaned_data['message']
            sender = contact_form.cleaned_data['email']
            subject = "[Pets First] New Message"
            recipients = settings.MANAGERS

            send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/')

    # For the sidebar categories menu list
    category = get_object_or_404(AdCategory, title='Pets For Sale')
    return render(request, 'contact_us.html', {
        'contact_form': contact_form,
        'category': category
    })


def all_adverts(request):
    adverts_list = Advert.objects.all_active_and_approved_ads() # return all approved adverts
    adverts_paginator = Paginator(adverts_list, settings.PAGINATOR_SIZE)

    page = request.GET.get('page')
    try:
        adverts = adverts_paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        adverts = adverts_paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        adverts = adverts_paginator.page(adverts_paginator.num_pages)

    return render(request, 'adverts/advert_listing.html', {'adverts': adverts})


def all_user_adverts(request, username):
    owner = get_object_or_404(get_user_model(), username=username)
    adverts_list = Advert.objects.get_user_ads(owner)
    adverts_paginator = Paginator(adverts_list, settings.PAGINATOR_SIZE)

    page = request.GET.get('page')
    try:
        adverts = adverts_paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        adverts = adverts_paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        adverts = adverts_paginator.page(adverts_paginator.num_pages)

    # For the sidebar categories menu list
    category = get_object_or_404(AdCategory, title='Pets For Sale')

    return render(request, 'adverts/advert_listing.html', {
                'adverts': adverts,
                'query': owner.username,
                'category': category})


@login_required
def view_dashboard(request):
    adverts = Advert.objects.filter(owner=request.user)
    return render(request, 'adverts/dashboard.html', {
                'adverts': adverts,
                'status': 'Active'})


@login_required
def view_expired_ads(request):
    adverts = Advert.objects.all_expired_ads(owner=request.user)
    return render(request, 'adverts/dashboard.html', {
                'adverts': adverts,
                'status': 'Expired'})


def getsubcategories(request):
    category_name = request.GET['cat']

    result_set = []
    all_subcategories = []

    selected_category = AdCategory.objects.filter(title=category_name)
    all_subcategories = AdCategory.objects.filter(parent=selected_category)
    for subcategory in all_subcategories:
        result_set.append({'id': subcategory.id, 'title': subcategory.title})
    return HttpResponse(json.dumps(result_set),
                        content_type='application/json'
    )


def search_ads(request):
    """
    Search for adverts matching the given criteria
    """
    if request.method == 'GET':
        search = request.GET.get('search_for', None)
        category = request.GET.get('search_cat', None)
        location = request.GET.get('search_cat', None)

        if category:
            # Lets get the current AdCategory object and get its children
            curr_cat = get_object_or_404(AdCategory, pk=category)
            sub_categories = AdCategory.objects.get_all_children(curr_cat)
        else:
            sub_categories = AdCategory.objects.all()

        adverts = Advert.objects.all_active_and_approved_ads().filter(title__icontains=search).filter(category__in=sub_categories)
        return render(request, 'adverts/advert_listing.html', {
            'adverts': adverts,
            'query': search,
            'category': category,
            'parent_categories': get_root_parent_categories()
        })


def all_category_ads(request, category_slug, cat_id):
    category = get_object_or_404(AdCategory, pk=cat_id)
    sub_categories = AdCategory.objects.get_all_children(category)
    adverts = Advert.objects.all_active_and_approved_ads().filter(category__in=sub_categories)
    return render(request, 'adverts/advert_listing.html', {
                'adverts': adverts,
                'query': category,
                'category': category})


def terms(request):
    return render(request, 'terms.html', {})


def about(request):
    return render(request, 'about.html', {})

