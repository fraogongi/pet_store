from django.conf.urls import patterns, url
from .views import AdvertWizard

urlpatterns = patterns('',
    url(r'^$', 'apps.adverts.views.all_adverts', name='all_adverts'),
    #url(r'^(\d+)/$', 'apps.adverts.views.view_advert', name='view_advert'),
    url(r'^(?P<advert_slug>[-\w\d]+),(?P<advert_id>\d+)/$', 'apps.adverts.views.view_advert', name='view_advert'),

    #url(r'^new$', 'apps.adverts.views.add_new_advert', name='new_advert'),
    url(r'^new$', AdvertWizard.as_view(), name='new_advert'),

    url(r'^update/(?P<advert_id>\d+)/$', 'apps.adverts.views.update_advert', name='update_advert'),
    url(r'^contact_seller/(?P<advert_id>\d+)/$', 'apps.adverts.views.contact_seller', name='contact_seller'),
    url(r'^search/$', 'apps.adverts.views.search_ads', name='search_ads'),
    url(r'^expired$', 'apps.adverts.views.view_expired_ads', name='expired_ads'),
    url(r'^user_ads/(?P<username>[-\w\d]+)/$', 'apps.adverts.views.all_user_adverts', name='user_ads'),
    url(r'^category/(?P<category_slug>[-\w\d]+),(?P<cat_id>\d+)/$', 'apps.adverts.views.all_category_ads', name='category_ads'),

    # Hitcount url to save hits on tutorial entity
    url(r'^ajax/hit/$', 'apps.adverts.views.update_hit_count_ajax', name='hitcount_update_ajax'),

    # AJAX Links
    url(r'^getsubcategories/', 'apps.adverts.views.getsubcategories'),
)
