from django import template
from apps.adverts.models import AdCategory

register = template.Library()

@register.inclusion_tag('tags/categories.html')
def get_category_list(category=None):
    categories = AdCategory.objects.filter(parent=category)
    return {'categories': categories}
