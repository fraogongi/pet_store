from django import template
from apps.adverts.models import AdCategory, Advert

register = template.Library()

@register.inclusion_tag('tags/slider_adverts.html')
def get_slider_adverts():
    # TODO: Ideally, adverts on the slider should be ones which have been 
    # paid for by the advertiser. Since this is a relatively new site, 
    # all adverts will be placed here until there is sufficient traffic.
    #slider_adverts = Advert.objects.get_premium_ads() 
    slider_adverts = Advert.objects.all()
    return {'slider_adverts': slider_adverts}
