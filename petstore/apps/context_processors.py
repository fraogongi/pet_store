from django.conf import settings
 
 
def global_settings(request):
    """
    This method is primarily used to make selected variables to be available 
    in the templates.
    """
    # return any necessary values
    return {
        'SITE_NAME': settings.SITE_NAME,
    }
