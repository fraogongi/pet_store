import re
import uuid
from dateutil.relativedelta import relativedelta

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core import validators
from django.utils import timezone
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class AuthUserManager(BaseUserManager):

    def _create_user(self, username, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, is_staff=is_staff, 
                 is_active=False, is_superuser=is_superuser, last_login=now,
                 date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using = self._db)
        return user
        
    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False, **extra_fields)
        
    def create_superuser(self, username, email, password, **extra_fields):
        user = self._create_user(username, email, password, True, True, **extra_fields)
        user.is_active = True
        user.save(using = self._db)
        return user


class AuthUser(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    username = models.CharField(_('username'), max_length=30, unique=True, 
        help_text=_('Required. 30 characters or fewer. Letters, numbers and @/./+/-/_ characters'), 
        validators=[validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'), _('invalid'))])
        
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.'))
                    
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
                    
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    
    objects = AuthUserManager()
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username'] # Not needed since it has been mentioned in USERNAME_FIELD and is required by default and cannot be listed in REQUIRED_FIELDS
    
    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        
    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)
        
    def __str__(self):
        return self.username
        
    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their username
        return self.username 
        
    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


def two_days_from_now():
    return timezone.now() + relativedelta(days=2)
 
def get_uuid_str():
    return uuid.uuid4().__str__()

    
class Registration(models.Model):
 
  uuid = models.CharField(max_length=36, default=get_uuid_str)
  # it might be possible the same user requests for a new password twice in a row so let's make this a ForeignKey
  user = models.ForeignKey(AuthUser, related_name='registration')
  expires = models.DateTimeField(default=two_days_from_now)
  type = models.CharField(max_length=10, choices=(
      ('register', 'register'),
      ('lostpass', 'lostpass'),
    ), default = 'register')
