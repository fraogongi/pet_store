from django.core.management.base import BaseCommand
from django.utils import timezone
 
from apps.users.models import AuthUser, Registration

 
class Command(BaseCommand):
    args = None
    help = 'Purges expired registrations'
    # ./manage.py purgeregistrations

    def handle(self, *args, **kwargs):
        #We only remove users related to expired registration processes
        AuthUser.objects.filter(registration__expires__lte=timezone.now()).filter(registration__type='register').delete()
        # We cleanup any other expired requests
        Registration.objects.filter(expires_lte=timezone.now()).delete()
