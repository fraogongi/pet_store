from __future__ import absolute_import
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserCreationForm, UserChangeForm, AuthenticationForm
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.forms import TextInput

from .models import AuthUser, Registration


class AuthUserCreationForm(UserCreationForm):
    """
    A form for creating new users, with no privileges, from the given email and
    password. Includes all the required fields, plus a repeated password.
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
    
    class Meta:
        model = AuthUser
        fields = ('username', 'email',)
        
    def clean_username(self):
        username = self.cleaned_data["username"]
        
        try:
            AuthUser._default_manager.get(username=username)
        except AuthUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])
    
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2
        
    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(AuthUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
        
        
class AuthUserChangeForm(UserChangeForm):
    """
    A form for updating users. Includes all the fields on the user, but 
    replaces the password field with admin's password hash display field.
    """
    username = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    password = ReadOnlyPasswordHashField(label="password",
                                         help_text="""Raw passwords are not stored, so there is no way to see this
                                         user's password, but you can change the password using <a href=\"password/\">
                                         this form</a>""")

    class Meta:
        model = AuthUser
        fields = ('username', 'email', 'password', 'is_active', 'is_staff', 'is_superuser', 'user_permissions')
        widgets = {
            'email': TextInput(),
        }

    def clean_username(self):
        # Regardless of what the user provides, reset field to initial value
        # Not appropriate to change usernames once set.
        return self.initial["username"]
        
    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the field does 
        # not have access to the initial value
        return self.initial["password"]
        
    def clean_is_active(self):
        return self.initial["is_active"]
        
        
class LoginFormMixin(object):
   
    # This is where our logic will take place

    username = forms.CharField(label = 'Username or e-mail', required=True)
    password  = forms.CharField(label = 'Password', widget = forms.PasswordInput, required = True)
 
    def clean(self):
        username = self.cleaned_data.get('username', '')
        password = self.cleaned_data.get('password', '')
        self.user = None
        users = AuthUser.objects.filter(Q(username=username)|Q(email=username))
        for user in users:
            if user.is_active and user.check_password(password):
                self.user = user
        if self.user is None:
            raise forms.ValidationError('Invalid username or password')
        # We are setting the backend here so you may (and should) remove the line setting it in apps.accountauth.views
        self.user.backend = 'django.contrib.auth.backends.ModelBackend'
        return self.cleaned_data
        
        
class LoginForm(forms.Form):                           
         
    login = forms.CharField(label = 'Username or e-mail', required=True)                                         
    password  = forms.CharField(label = 'Password', widget = forms.PasswordInput, required = True)
       
    def clean(self):                                     
        login = self.cleaned_data.get('login', '')         
        password = self.cleaned_data.get('password', '')
        self.user = None
        users = get_user_model().objects.filter(Q(username=login)|Q(email=login))
        for user in users:
            if user.is_active and user.check_password(password):
                self.user = user
        if self.user is None:
            raise forms.ValidationError('Invalid username or password')
        return self.cleaned_data
        
        
class AuthLoginForm(LoginFormMixin, AuthenticationForm):
 
    # Because of the way Python's MRO works we have to redefine username which has been overridden by AuthenticationForm
    username = forms.CharField(label = 'Username or e-mail', required=True)
    this_is_the_login_form = forms.BooleanField(widget=forms.HiddenInput, initial=1, error_messages={'required': "Please log in again, because your session has expired."})
 
    def clean(self):
        cleaned_data = super(AuthLoginForm, self).clean()
        if self.user is not None:
            self.user_cache = self.user
        return cleaned_data
        
        
class UserRegistrationForm(AuthUserCreationForm):
 
    class Meta:
        # Since we are overriding Meta we have to re-tell it the model also
        model = AuthUser
        exclude = ['password', 'last_login', 'date_joined', 'groups', 'user_permissions', 'is_staff', 'is_superuser', 'is_active']
        
        
class LostPasswordForm(forms.Form):
 
    email = forms.EmailField(label='E-mail address')
     
    def clean(self):
        cleaned_data = super(LostPasswordForm, self).clean()
        try:
            user = AuthUser.objects.get(email=cleaned_data['email'], is_active=True)
        except User.DoesNotExist:
            raise forms.ValidationError("We don't know of any user with that e-mail address")
        return cleaned_data
        

class LostPasswordChangeForm(forms.ModelForm):
 
    id = forms.IntegerField(widget=forms.HiddenInput())
    password1 = forms.CharField(label="Password", min_length=8, widget=forms.PasswordInput())
    password2 = forms.CharField(label="Password confirmation", widget=forms.PasswordInput())
 
    class Meta:
        model = Registration
        fields = ['id',]    
 
    def clean_password2(self):
        cleaned_data = super(LostPasswordChangeForm, self).clean()
        self.password = cleaned_data.get('password1')
        if self.password != cleaned_data.get('password2'):
            raise forms.ValidationError('Password do not match')
        self.user = self.instance.user
        return self.password
        
