from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.views.generic import UpdateView
from django.views.generic.base import RedirectView, TemplateView        
from django.views.generic.edit import FormView         
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.sites.models import Site
from django.shortcuts import render
from django.template import loader, Context
       
from .forms import LoginForm, UserRegistrationForm, LostPasswordForm, LostPasswordChangeForm, AuthUserChangeForm
from .models import AuthUser, Registration


class LoginRequiredMixin(object):

    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)
        

class LoginView(FormView):
     
    template_name = 'users/login.html'
    form_class = LoginForm
    
    def get_success_url(self):
        next_url = self.request.GET.get('next', None)
        if next_url:
            return "%s" % (next_url)
        else:
            return reverse('home')
   
    def form_valid(self, form):
        form.user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, form.user)
        #messages.info(self.request, 'Login successful')
        return super(LoginView, self).form_valid(form)
        
        
class LogoutView(RedirectView):
                                                        
    permanent = False
                                                        
    def get_redirect_url(self):
        return reverse('login')
 
    def dispatch(self, request, *args, **kwargs):
        try:
            logout(request)
        except:
            ## if we can't log the user out, it probably means they we're not logged-in to begin with, so we do nothing
            pass
        #messages.info(request, 'You have successfully logged out. Come back soon.')
        return super(LogoutView, self).dispatch(request, *args, **kwargs)
        
        
class RegisterView(FormView):

    template_name = 'users/register.html'
    form_class = UserRegistrationForm
    
    def get_success_url(self):
        return reverse('confirmation_mail_sent')
        
    def form_valid(self, form):
        user = form.save()
        registration = Registration.objects.create(user=user)
        #messages.info(self.request, 'Registration successful.')
        
        # Send email to user
        t = loader.get_template('templated_email/registration.txt')
        c = Context({
            'name': user.username,
            'product_name': settings.SITE_NAME,
            'product_url': 'http://{}/'.format(settings.SITE_URL),
            'login_url': 'http://{}/login/'.format(settings.SITE_URL),
        })
        subject = 'Welcome to {}'.format(settings.SITE_NAME)
        #send_mail(subject, t.render(c), 'from@address.com', [new_data['email']], fail_silently=False)        
        user.email_user(subject, t.render(c), settings.DEFAULT_FROM_EMAIL)
        
        # TODO: Send email below to user
        """send_templated_mail(
            template_name='registration',
            # substitute your e-mail adress
            from_email='fraogongi@gmail.com',
            recipient_list=[form.cleaned_data['email'],],
            context = {
                'url_name': 'activation',
                'url_param': 'key',
                'registration': registration,
                # make sure SITE_URL is defined in your settings.py
                'base_url': Site.objects.get_current().domain, #settings.SITE_URL,
            },
        )"""
        return super(RegisterView, self).form_valid(form)
        
        
class EmailSentView(TemplateView):
 
    template_name = 'users/email_sent.html'
    
    
class ActivationView(RedirectView):
 
    permanent = False
 
    def get_redirect_url(self):
        return reverse('login')
 
    def get(self, request, *args, **kwargs):
        uuid = request.GET.get('key', None)
        if uuid is None:
            raise Http404
        try:
            user = AuthUser.objects.get(registration__uuid=uuid, type='register')
            user.is_active = True
            user.save()
            user.registration.delete()
            #messages.info(self.request, 'User activation successfull')
        except:
            raise Http404
        return super(ActivationView, self).get(request, *args, **kwargs)
        
        
class UpdateUserView(LoginRequiredMixin, UpdateView):

    template_name = 'users/update_user.html'
    form_class = AuthUserChangeForm
    
    # get current user object
    def get_object(self, queryset=None): 
        return self.request.user
        
    def get_success_url(self):
        return reverse('dashboard')
        
       
class LostPasswordView(FormView):
 
    template_name = 'users/lost_password.html'
    form_class = LostPasswordForm
 
    def get_success_url(self):
        return reverse('lost_password_mail_sent')
 
    def form_valid(self, form):
        user = form.user
        registration = Registration.objects.create(user=user, type='lostpass')
        """send_templated_mail(
            template_name='lost_password',
            # substitute your e-mail adress
            from_email = 'fraogongi@gmail.com',
            recipient_list=[form.cleaned_data['email'],],
            context={
                'url_name': 'reset_password',
                'url_param': 'key',
                'registration': registration,
                # make sure SITE_URL is defined in your settings.py
                'base_url': Site.objects.get_current().domain, #settings.SITE_URL,
            },
        ) """
        return super(LostPasswordView, self).form_valid(form)
        
        
class LostPasswordEmailSentView(TemplateView):
 
    template_name = 'users/lost_password_email_sent.html'
    
    
class LostPasswordChangeView(FormView):
 
    template_name = 'users/lost_password_change.html'
    form_class = LostPasswordChangeForm
 
    def get_success_url(self):
        return reverse('login')
   
    def get_form_kwargs(self):
        kwargs = super(LostPasswordChangeView, self).get_form_kwargs()
        if self.request.method == 'GET':
            key = self.request.GET.get('key')
            try:
                registration = Registration.objects.get(uuid=key, type='lostpass')
            except:
                raise Http404
        else:
            try: 
                registration = Registration.objects.get(pk=kwargs['data'].get('id'), type='lostpass')
            except:
                raise Http404
        kwargs['instance'] = registration
        return kwargs
 
    def form_valid(self, form):
        form.user.set_password(form.password)
        form.user.save()
        form.instance.delete()
        #messages.info(self.request, 'Your password has been successfully updated')
        return super(LostPasswordChangeView, self).form_valid(form)
