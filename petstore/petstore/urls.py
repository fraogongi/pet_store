from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView

from accountusers.forms import AuthLoginForm
from accountusers.views import LoginView, LogoutView, RegisterView, EmailSentView, ActivationView, LostPasswordView, LostPasswordEmailSentView, LostPasswordChangeView , UpdateUserView

urlpatterns = patterns('',
    url(r'^$', 'apps.adverts.views.home_page', name='home'),
    url(r'^ads/', include('apps.adverts.urls')),
    
    url(r'^contact/', 'apps.adverts.views.contact_us', name='contact_us'),
    url(r'^dashboard/', 'apps.adverts.views.view_dashboard', name='dashboard'),
    
    # user authentication and registration
    url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name='logout'),
    url(r'^register/', RegisterView.as_view(), name='register'),
    url(r'^profile/update/', UpdateUserView.as_view(), name='update_profile'),
    url(r'^registration_email_sent\.html', EmailSentView.as_view(), name='confirmation_mail_sent'),
    url(r'^user_activation\.html', ActivationView.as_view(), name='activation'),
    
    # user password handling
    url(r'^lost_password', LostPasswordView.as_view(), name='lost_password'),
    url(r'^lost_password_mail_sent', LostPasswordEmailSentView.as_view(), name='lost_password_mail_sent'),
    url(r'^reset_password', LostPasswordChangeView.as_view(), name='reset_password'),    
    url(r'^password_change/$', 
        'django.contrib.auth.views.password_change', 
        {'template_name': 'users/password_change_form.html'},
        name="password_change"),
    url(r'^password_change_done/$', 
        'django.contrib.auth.views.password_change_done', 
        {'template_name': 'users/password_change_done.html'},
        name="password_change_done"),
    
    # Static html files and other dynamic pages
    url(r'^about/', 'apps.adverts.views.about', name='about'),
    url(r'^terms/', 'apps.adverts.views.terms', name='terms'),
    url(r'^services/', 'apps.adverts.views.services', name='services'),
    url(r'^products/', 'apps.adverts.views.products', name='products'),
    
    # static files (images, css, javascript, etc.)
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    
    # robots.txt file
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),

    url(r'^admin/', include(admin.site.urls)),
)

# Overriding Django admin login form
admin.site.login_form = AuthLoginForm
admin.autodiscover()
