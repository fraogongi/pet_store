"""
WSGI config for petstore project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys, site

# Tell wsgi to add the Python site-packages to its path. 
site.addsitedir('/home/fraogongi/webapps/pets_app/lib/python3.4/site-packages')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.production")

# Calculate the path based on the location of the WSGI script
project = '/home/fraogongi/webapps/pets_app/pet_project/petstore/'
workspace = os.path.dirname(project)
sys.path.append(workspace)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
