"""
Django settings for petstore project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8cn+tr4$fyo%xfyzlu#m!fee9eo(3t96t&qr3qmnk%n5lc-7&0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    'apps.adverts',
    'accountusers', # Put outside the apps folder for AUTH_USER_MODEL errors
    
    # Required for the form wizard
    'django.contrib.formtools',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'petstore.urls'

WSGI_APPLICATION = 'petstore.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Africa/Nairobi'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

# ======================= OVERRIDES ===========================================

# Our static directory for CSS, JS and images
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

# Our templates directory
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

# Our uploaded photos DIR
MEDIA_ROOT = (
    os.path.join(BASE_DIR,  'media')
)
MEDIA_URL = '/media/'
MAX_UPLOAD_SIZE = 5571520  # 5MB
CONTENT_TYPES = ['application/pdf', 'image/jpeg', 'image/png']  # .pdf, .jpeg and .png

# Overriding the default user model
AUTH_USER_MODEL = 'accountusers.AuthUser'

# Needed to allow login_required() decorator to work correctly.
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

# Expire the user sessions
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_AGE = 1200 # Expire after 20 minutes

# The number of days, weeks, months, hours, etc (timedelta kwargs), that a Hit 
# is kept 'active'. If a Hit is 'active', a repeat viewing will not be counted. 
# After the active period ends, however, a new Hit will be recorded. 
# You can decide how long you want this period to last .
# Set to 90 days since most ads will have expired by then.
HITCOUNT_KEEP_HIT_ACTIVE = { 'days': 90 }
HITCOUNT_KEEP_HIT_IN_DATABASE = { 'days': 90 }

# Expiration duration of an Advert. Default is 90 days (3 months)
ADVERT_VALID_DAYS = 90

# Send email to these people when an error occurs
ADMINS = [
    'fraogongi@gmail.com',
    'info@petsfirst.co.ke',
    'candidoakconcepts@gmail.com',
]
MANAGERS = ADMINS

SITE_NAME = "PetsFirst"
SITE_URL = "www.petsfirst.co.ke"
DEFAULT_FROM_EMAIL = 'info@petsfirst.co.ke'
EMAIL_SUBJECT_PREFIX = "[PetsFirst] "

# SEO
META_KEYWORDS = ""
META_DESCRIPTION = ""

# Make setting variables accessible in templates
import django.conf.global_settings as DEFAULT_SETTINGS
TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
    'apps.context_processors.global_settings',
)

# Number of entries to show per page (Pagination)
PAGINATOR_SIZE = 30

