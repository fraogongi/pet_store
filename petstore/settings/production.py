from .common import *
import socket
import os

def contains(str, substr):
    if str.find(substr) != -1:
        return True
    else:
        return False

if contains(socket.gethostname(), 'webfaction'):
    LIVEHOST = True
else:
    LIVEHOST = False
    
if LIVEHOST:

    DEBUG = TEMPLATE_DEBUG = False

    EMAIL_HOST = 'smtp.webfaction.com'
    EMAIL_PORT = '25'
    EMAIL_HOST_USER = 'petsadmin'
    EMAIL_HOST_PASSWORD = 'sphinx1982'
    DEFAULT_FROM_EMAIL = 'info@petsfirst.co.ke'
    SERVER_EMAIL = 'info@petsfirst.co.ke'

    MEDIA_ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), '../../../media/').replace('\\','/'))
    STATIC_ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), '../../../static/').replace('\\','/'))
    
    STATICFILES_DIRS = (
        os.path.join(BASE_DIR, "static"),
        '/home/fraogongi/webapps/pets_app/pet_project/petstore/static/',
    )

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'petstoredb',
            'USER': 'petuser',
            'PASSWORD': 'Br@ve2015',
            'HOST': 'localhost',
            'PORT': '',
        },
    }
    
    # Needed when DEBUG = False
    ALLOWED_HOSTS = ['petsfirst.co.ke', 'www.petsfirst.co.ke']
